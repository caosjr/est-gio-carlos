#include <stdio.h>
#include <stdlib.h>
//#include <sys/resource.h>
#include <sys/time.h>
#include "lp_lib.h"
#define TAM 10000

int objective[TAM];

double time_diff(struct timeval x , struct timeval y)
{
    double x_ms , y_ms , diff;

    x_ms = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_ms = (double)y.tv_sec*1000000 + (double)y.tv_usec;

    diff = (double)y_ms - (double)x_ms;

    return diff;
}

/*void increasesize()
{
    //aumentar o tamanho da stack
    const rlim_t kStackSize = 64L * 1024L * 1024L;   // min stack size = 64 Mb
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
        if (rl.rlim_cur < kStackSize)
        {
            rl.rlim_cur = kStackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0)
            {
                fprintf(stderr, "setrlimit returned result = %d\n", result);
            }
        }
    }
}*/

void readFile(char *filePath){
    int m, n;
    FILE *fp, *mod;

    fp = fopen(filePath, "r");
    mod = fopen("modificado.lp", "w");
    if (fp == NULL || mod == NULL) {
        perror("Error");
        exit(1);
    }

    fscanf(fp,"%d",&m);
    fscanf(fp,"%d",&n);
    m++;

    memset(objective, 0, sizeof(objective));
    int i = 0, j = 0, x = 0;
    char name[50];
    for (j = 0; j < n; j++){
        fscanf(fp, "%d", &x);
        objective[j] = x;
        memset(name, ' ', sizeof(name));
        if (j != n-1){
            sprintf(name, "%d%s%d + ", x, "x", j+1);
        } else {
            sprintf(name, "%d%s%d;", x, "x", j+1);
        }

        if (j == 0){
            fprintf(mod, "min: %s", name);
        } else {
            fprintf(mod, "%s", name);
        }
    }
    fprintf(mod, "\n");

    i = 1;
    while (i < m){
        fscanf(fp,"%d",&x);
        fscanf(fp,"%d",&j);
        while (x > 0){
            memset(name, ' ', sizeof(name));
            if (x > 1){
                sprintf(name, "%s%d + ", "x", j);
            } else if (x == 1){
                sprintf(name, "%s%d >= 1;\n", "x", j);
            }
            fprintf(mod, "%s", name);

            if (x > 1) fscanf(fp,"%d",&j);
            x--;
        }
        i++;
    }
    for (i = 0; i < n; i++){
        memset(name, ' ', sizeof(name));
        if (i == 0){
            sprintf(name, "sec %s%d, ","x", i+1);
        } else if (i == n-1){
            sprintf(name, "%s%d;","x", i+1);
        } else{
            sprintf(name, "%s%d, ","x", i+1);
        }
        fprintf(mod, "%s", name);
    }
    fclose(fp);
    fclose(mod);

    /*gambiarra
    int cont = 0, maior = 0;
    for(i = 1; i < m; i++){
        for (j = 0; j < n; j++){
            if (arrayFile[i][j] != 0){
                    cont++;
            }
        }
        if (cont > maior){
            maior = cont;
        }
        cont = 0;
    }*/
}

int main()
{
    //increasesize();
    char filePath[60] = "/home/caosjr/Projetos C/arredondamento/problems/scpnrh5.txt";
    readFile(filePath);
    FILE *save;
    lprec *lp;
    REAL var[TAM];

    lp = read_LP("modificado.lp", FULL, "test model");
    save = fopen("/home/caosjr/Projetos C/arredondamento/results/result_rounding_all_scpnrh5.txt", "w");
    if(lp == NULL && save == NULL) {
        fprintf(stderr, "Unable to read model\n");
        return(1);
    }
    struct timeval before , after;
    gettimeofday(&before , NULL);
    solve(lp);
    //print_solution(lp, TAM);
    get_variables(lp, var);//variaveis resolvidas

    int i = 0, aux = 0;
    for (i = 0; i < TAM; i++){
        if (var[i] > 0){
            aux += objective[i];
            fprintf(save, "%d ", i+1);
        }
    }
    gettimeofday(&after , NULL);
    fprintf(save, "\n%d", aux);
    printf("Custo: \n%d", aux);
    printf("\nTotal time elapsed : %.0lf us\n" , time_diff(before , after));
    print_objective(lp);

    //write_basis(lp, "model.bas");
    delete_lp(lp);
    return 0;
}
