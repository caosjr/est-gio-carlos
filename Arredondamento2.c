#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "lp_lib.h"
#define TAM 10000

int objective[TAM];

double time_diff(struct timeval x , struct timeval y)
{
    double x_ms , y_ms , diff;

    x_ms = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_ms = (double)y.tv_sec*1000000 + (double)y.tv_usec;

    diff = (double)y_ms - (double)x_ms;

    return diff;
}

void readFile(char *filePath){
    int m, n;
    FILE *fp, *mod;

    fp = fopen(filePath, "r");
    mod = fopen("modificado.lp", "w");
    if (fp == NULL || mod == NULL) {
        perror("Error");
        exit(1);
    }

    fscanf(fp,"%d",&m);
    fscanf(fp,"%d",&n);
    m++;

    memset(objective, 0, sizeof(objective));
    int i = 0, j = 0, x = 0;
    char name[50];
    for (j = 0; j < n; j++){
        fscanf(fp, "%d", &x);
        objective[j] = x;
        memset(name, ' ', sizeof(name));
        if (j != n-1){
            sprintf(name, "%d%s%d + ", x, "x", j+1);
        } else {
            sprintf(name, "%d%s%d;", x, "x", j+1);
        }

        if (j == 0){
            fprintf(mod, "min: %s", name);
        } else {
            fprintf(mod, "%s", name);
        }
    }
    fprintf(mod, "\n");

    i = 1;
    while (i < m){
        fscanf(fp,"%d",&x);
        fscanf(fp,"%d",&j);
        while (x > 0){
            memset(name, ' ', sizeof(name));
            if (x > 1){
                sprintf(name, "%s%d + ", "x", j);
            } else if (x == 1){
                sprintf(name, "%s%d >= 1;\n", "x", j);
            }
            fprintf(mod, "%s", name);

            if (x > 1) fscanf(fp,"%d",&j);
            x--;
        }
        i++;
    }
    for (i = 0; i < n; i++){
        memset(name, ' ', sizeof(name));
        if (i == 0){
            sprintf(name, "sec %s%d, ","x", i+1);
        } else if (i == n-1){
            sprintf(name, "%s%d;","x", i+1);
        } else{
            sprintf(name, "%s%d, ","x", i+1);
        }
        fprintf(mod, "%s", name);
    }
    fclose(fp);
    fclose(mod);
}

int main()
{
    char filePath[60] = "/home/caosjr/Projetos C/arredondamento2/problems/scpnrh5.txt";
    readFile(filePath);
    FILE *save;
    lprec *lp;
    REAL var[TAM];

    lp = read_LP("modificado.lp", FULL, "test model");
    save = fopen("/home/caosjr/Projetos C/arredondamento2/results/result_rounding_algorithm_scpnrh5.txt", "w");
    float F = 578;
    float const_f = 1/F;
    if(lp == NULL && save == NULL) {
        fprintf(stderr, "Unable to read model\n");
        return(1);
    }

    struct timeval before , after;
    gettimeofday(&before , NULL);
    solve(lp);
    get_variables(lp, var);//variaveis resolvidas

    int i = 0, aux = 0;
    for (i = 0; i < TAM; i++){
        if (var[i] >= const_f){
            aux += objective[i];
            fprintf(save, "%d ", i+1);
        }
    }
    gettimeofday(&after , NULL);
    fprintf(save, "\n%d", aux);
    printf("Custo: \n%d", aux);
    printf("\nTotal time elapsed : %.0lf us\n" , time_diff(before , after));
    print_objective(lp);
    delete_lp(lp);
    return 0;
}
