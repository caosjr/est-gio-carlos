#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/time.h>
#include "lp_lib.h"
#define TAM 10000

int objective[TAM];

double time_diff(struct timeval x , struct timeval y)
{
    double x_ms , y_ms , diff;

    x_ms = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_ms = (double)y.tv_sec*1000000 + (double)y.tv_usec;

    diff = (double)y_ms - (double)x_ms;

    return diff;
}

void increasesize()
{
    //aumentar o tamanho da stack
    const rlim_t kStackSize = 64L * 1024L * 1024L;   // min stack size = 64 Mb
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
        if (rl.rlim_cur < kStackSize)
        {
            rl.rlim_cur = kStackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0)
            {
                fprintf(stderr, "setrlimit returned result = %d\n", result);
            }
        }
    }
}

void readFile(char *filePath){
    int m, n;
    FILE *fp, *mod;

    fp = fopen(filePath, "r");
    mod = fopen("modificado.lp", "w");
    if (fp == NULL || mod == NULL) {
        perror("Error");
        exit(1);
    }

    fscanf(fp,"%d",&m);
    fscanf(fp,"%d",&n);
    m++;

    memset(objective, 0, sizeof(objective));
    int i = 0, j = 0, x = 0;
    char name[50];
    for (j = 0; j < n; j++){
        fscanf(fp, "%d", &x);
        objective[j] = x;
        memset(name, ' ', sizeof(name));
        if (j != n-1){
            sprintf(name, "%d%s%d + ", x, "x", j+1);
        } else {
            sprintf(name, "%d%s%d;", x, "x", j+1);
        }

        if (j == 0){
            fprintf(mod, "min: %s", name);
        } else {
            fprintf(mod, "%s", name);
        }
    }
    fprintf(mod, "\n");

    i = 1;
    while (i < m){
        fscanf(fp,"%d",&x);
        fscanf(fp,"%d",&j);
        while (x > 0){
            memset(name, ' ', sizeof(name));
            if (x > 1){
                sprintf(name, "%s%d + ", "x", j);
            } else if (x == 1){
                sprintf(name, "%s%d >= 1;\n", "x", j);
            }
            fprintf(mod, "%s", name);

            if (x > 1) fscanf(fp,"%d",&j);
            x--;
        }
        i++;
    }
    for (i = 0; i < n; i++){
        memset(name, ' ', sizeof(name));
        if (i == 0){
            sprintf(name, "sec %s%d, ","x", i+1);
        } else if (i == n-1){
            sprintf(name, "%s%d;","x", i+1);
        } else{
            sprintf(name, "%s%d, ","x", i+1);
        }
        fprintf(mod, "%s", name);
    }
    fclose(fp);
    fclose(mod);
}

int main()
{
    increasesize();
    char filePath[80] = "/home/caosjr/Projetos C/arredondamentoRandomico/problems/scpnrh5.txt";
    readFile(filePath);
    int m, n;
    FILE *fp, *save;
    lprec *lp;
    REAL var[TAM];

    lp = read_LP("modificado.lp", FULL, "test model");
    fp = fopen(filePath, "r");
    save = fopen("/home/caosjr/Projetos C/arredondamentoRandomico/results/result_randomized_rounding_scpnrh5.txt", "w");
    if(lp == NULL && save == NULL && fp == NULL) {
        fprintf(stderr, "Unable to read model\n");
        return(1);
    }
    fscanf(fp,"%d",&m);
    fscanf(fp,"%d",&n);
    m++;
    int arrayFile[m][n];
    memset(arrayFile, 0, sizeof(arrayFile)); //limpar o array

    int i = 0, j = 0, x = 0;
    for (j = 0; j < n; j++){
        fscanf(fp,"%d",&arrayFile[i][j]);
    }

    i = 1;
    while (i < m){
        fscanf(fp,"%d",&x);
        fscanf(fp,"%d",&j);
        while (x > 0){
            arrayFile[i][j-1] = -1;
            if (x > 1) fscanf(fp,"%d",&j);
            x--;
        }
        i++;
    }
    fclose(fp);

    struct timeval before , after;
    gettimeofday(&before , NULL);
    solve(lp);
    get_variables(lp, var);//variaveis resolvidas

    int cost = 0;
    double r = 0;
    int resultado[TAM], teste[TAM];
    int stop = 0, cont = 0, lines;

    while(cont < 4 && stop == 0){
        for (i = 0; i < n; i++){
            r = (double)(1.0/(RAND_MAX + 1.0)) * ((double)(rand()));
            r = r/100;
            if (var[i] >= r){
                cost += objective[i];
                resultado[i] = 1;
            }
        }

        //Verificador de solução
        memset(teste, 0, sizeof(teste));
        for(j = 0; j < n; j++){
            if (resultado[j] == 1){
                for (i = 1; i < m; i++){
                    if (arrayFile[i][j] != 0 && teste[i-1] == 0){
                        teste[i-1] = 1;
                        lines++;
                    }
                }
            }
        }
        if (lines == m-1){
            stop = 1;
        } else {
            cont++;
        }
    }
    fprintf(save, "%d\n", cost);
    for (i = 0; i < n; i++){
        if (resultado[i] == 1) fprintf(save, "%d ", i+1);
    }


    gettimeofday(&after , NULL);
    printf("\n\nCusto: %d", cost);
    printf("\nTempo Corrido: %.0lf us\n" , time_diff(before , after));
    printf ("Quantidade de vezes: %d", cont+1);

    delete_lp(lp);
    return 0;
}
