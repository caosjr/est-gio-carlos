#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<string.h>
#include <sys/resource.h>
#include <sys/time.h>

double time_diff(struct timeval x , struct timeval y)
{
    double x_ms , y_ms , diff;

    x_ms = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_ms = (double)y.tv_sec*1000000 + (double)y.tv_usec;

    diff = (double)y_ms - (double)x_ms;

    return diff;
}

void increasesize()
{
    //aumentar o tamanho da stack
    const rlim_t kStackSize = 64L * 1024L * 1024L;   // min stack size = 64 Mb
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
        if (rl.rlim_cur < kStackSize)
        {
            rl.rlim_cur = kStackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0)
            {
                fprintf(stderr, "setrlimit returned result = %d\n", result);
            }
        }
    }
}

int main()
{
    increasesize();
    int m, n;
    FILE *fp, *save;

    fp = fopen("/home/caosjr/Projetos C/orLibrary/problems/scpnrh5.txt", "r");
    save = fopen("/home/caosjr/Projetos C/orLibrary/results/result_scpnrh5.txt", "w");
    if (fp == NULL && save == NULL) {
        perror("Error");
        exit(1);
    }
    fscanf(fp,"%d",&m);
    fscanf(fp,"%d",&n);
    m++;
    int arrayFile[m][n];
    memset(arrayFile, 0, sizeof(arrayFile)); //limpar o array

    int i = 0, j = 0, x = 0;
    for (j = 0; j < n; j++){
        fscanf(fp,"%d",&arrayFile[i][j]);
    }

    i = 1;
    while (i < m){
        fscanf(fp,"%d",&x);
        fscanf(fp,"%d",&j);
        while (x > 0){
            arrayFile[i][j-1] = -1;
            if (x > 1) fscanf(fp,"%d",&j);
            x--;
        }
        i++;
    }
    /*for (i = 0; i < m; i ++){
        for (j = 0; j < n; j++){
            printf("%d\t", arrayFile[i][j]);
        }
        printf ("\n");
    }*/

    fclose(fp);

    //gambiarra
    int cont = 0, maior = 0;
    for(i = 1; i < m; i++){
        for (j = 0; j < n; j++){
            if (arrayFile[i][j] != 0){
                    cont++;
            }
        }
        if (cont > maior){
            maior = cont;
        }
        cont = 0;
    }

    //Aqui come�a o algoritmo esquema primal dual
    struct timeval before , after;
    gettimeofday(&before , NULL);
    int e = 0, ye = 2147483647, y[m-1], elements = 0, sum[n], sets[n];
    memset(y, 0, sizeof(y));
    memset(sets, 0, sizeof(sets));


    for (j = 0; j < n; j++){ //Inicializa o vetor de soma
        sum[j] = arrayFile[0][j];
    }
    while (elements < m-1){ //until all elements are covered, do:
        ye = 2147483647;
        for (i = 0; i < m; i++){ //pic an uncovered element, say e, and raise ye until some set goes tight
            if (y[i] == 0){
                e = i+1;
                break;
            }
        }
        for (j = 0; j < n; j++){
            if (arrayFile[e][j] != 0){
                if (sum[j] < ye){//procura o m�ximo que pode ser aumentado sem quebrar nenhuma restri��o
                    ye = sum[j];
                    //setCover = j; //indice da restri��o
                }
            }
        }
        for (j = 0; j < n; j++){ //Pick all tight sets in the cover and update x
            if (arrayFile[e][j] != 0){
                sum[j] = sum[j] - ye;
                if (sum[j] == 0){
                    sets[j] = 1;
                    for (i = 1; i < m; i++){ //declare all the elements occuring in these sets as "covered"
                        if((arrayFile[i][j] != 0) && (y[i-1] == 0)){
                            y[i-1] = 1;
                            elements++;
                        }
                    }
                }
            }
        }
    }
    gettimeofday(&after , NULL);

    int aux = 0;
    for(i = 0; i < n; i++){
        if (sets[i] == 1){
            aux += arrayFile[0][i];
        }
    }

    fprintf(save, "%d\n", aux);
    fprintf(save, "%d\n", maior);
    for (i = 0; i < n; i++){
        if (sets[i] == 1){
            fprintf(save, "%d ", i+1);
        }
    }
    fclose(save);

    printf("Custo: %d\n", aux);
    printf("f: %d\n", maior);
    printf("Total time elapsed : %.0lf us\n" , time_diff(before , after));

    printf("\nresultado\n\n");
    for (i = 0; i < n; i++){
        if (sets[i] != 0) printf("%d\t", i+1);
    }

    //Verificador de solu��o
    int teste[n];
    memset(teste, 0, sizeof(teste));
    cont = 0;
    for(j = 0; j < n; j++){
        if (sets[j] == 1){
            for (i = 1; i < m; i++){
                if (arrayFile[i][j] != 0 && teste[i-1] == 0){
                    teste[i-1] = 1;
                    cont++;
                }
            }
        }
    }
    if (cont == m-1){
        printf("\nPassou no teste, todas as linhas foram cobertas. Cont = %d e m = %d", cont, m-1);
    } else {
        printf ("o resultado est� errado. Cont = %d e m = %d", cont, m-1);
    }

    return 0;
}
